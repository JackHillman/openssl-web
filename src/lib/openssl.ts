// @ts-ignore
import defaultWasmPath from '../wasm/openssl'

// @ts-ignore
import configFile from '../wasm/openssl.cnf'

import { WASI } from '@wasmer/wasi'
import { WasmFs } from '@wasmer/wasmfs'

export default class OpenSSL {
  private wasmFs?: WasmFs

  private _wasi?: WASI

  constructor(private wasmPath: string = defaultWasmPath) {
    //
  }

  public async run(args: string[]) {
    const wasi = await this.wasi(args)
    const instance = await this.instance(args)
    const { fs } = await this.fs()

    try {
      wasi.start(instance)
    } catch (e) {
      if (e.code !== 0) {
        throw e
      }
    }

    const stdout = fs.readFileSync('/dev/stdout', 'UTF-8')
    const stderr = fs.readFileSync('/dev/stderr', 'UTF-8')

    return { stdout, stderr, fs }
  }

  private async instance(args: string[]) {
    const module = await this.module()
    const wasi = await this.wasi(args)

    return WebAssembly.instantiate(module, { ...wasi.getImports(module) })
  }

  private async module(): Promise<WebAssembly.Module> {
    const response = await fetch(this.wasmPath)
    const responseArrayBuffer = await response.arrayBuffer()
    const bytes = new Uint8Array(responseArrayBuffer).buffer

    return WebAssembly.compile(bytes)
  }

  private async fs(): Promise<WasmFs> {
    if (this.wasmFs) {
      return this.wasmFs
    }

    this.wasmFs = new WasmFs()
    const config = await fetch(configFile).then(res => res.text())

    this.wasmFs.fs.writeFileSync('/openssl.cnf', config)

    return this.wasmFs
  }

  private async wasi(args: string[]): Promise<WASI> {
    if (this._wasi) {
      return this._wasi
    }

    const { fs } = await this.fs()

    this._wasi = new WASI({
      args,
      env: {},
      preopenDirectories: {
        '/': '/',
      },
      bindings: {
        ...WASI.defaultBindings,
        fs,
      }
    })

    return this._wasi
  }
}


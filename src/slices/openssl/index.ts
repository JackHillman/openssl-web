import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'

const initialState: {
  loading: boolean
  cert?: string
  privateKey?: string
} = {
  loading: true,
  cert: undefined,
  privateKey: undefined,
}

const reducers: SliceCaseReducers<typeof initialState> = {
  hasLoaded: state => ({ ...state, loaded: true }),
  setCert: (state, { payload }) => ({ ...state, cert: payload }),
  setKey: (state, { payload }) => ({ ...state, privateKey: payload }),
  load: (state, { payload }) => ({ ...payload }),
}

const opensslSlice = createSlice({
  name: 'openssl',
  initialState,
  reducers,
})

export default opensslSlice

export const {
  hasLoaded,
  setCert,
  setKey,
  load,
} = opensslSlice.actions

import { combineReducers } from 'redux'
import { openssl } from '../slices'

const rootReducer = combineReducers({
  openssl: openssl.reducer,
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer

import React, { FC, useEffect } from 'react'
import OpenSSL from '../../lib/openssl'
import { connect, ConnectedProps } from 'react-redux'
import { RootState } from '../../store/rootReducer'
import { load } from '../../slices/openssl'

const connector = connect((state: RootState) => ({
  ...state.openssl,
}), {
  load,
})

const App: FC<ConnectedProps<typeof connector>> = ({ loading, cert, privateKey, load }) => {
  useEffect(() => {
    const subject = [
      'C=AU',
      'ST=SA',
      'L=Adelaide',
      'O=Example Org',
      'OU=NOC',
      'CN=example.com',
      'emailAddress=example@example.com',
    ]

    const args = [
      'req', '-x509', '-nodes',
      '-days', '365',
      '-newkey', 'rsa:2048',
      '-config', '/openssl.cnf',
      '-keyout', '/localhost.key',
      '-out', '/localhost.cert',
      '-subj', `/${subject.join('/')}`
    ]

    const openssl = new OpenSSL()

    openssl.run(args).then(({ fs }) => {
      const privateKey = fs.readFileSync('/localhost.key', 'UTF-8') as string
      const cert = fs.readFileSync('/localhost.cert', 'UTF-8') as string

      load({
        privateKey,
        cert,
        loading: false,
      })
    })
  }, [load])

  const OutputWrapper: FC<{
    title?: string
    value?: string
  }> = ({ title, value }) => (
    <div>
      <strong>
        {title}
      </strong>
      <pre>
        {value}
      </pre>
    </div>
  )

  return loading ? (
    <>
      {'Loading...'}
    </>
  ) : (
    <div>
      <OutputWrapper
        title={'Private Key'}
        value={privateKey} />
      <OutputWrapper
        title={'Certificate'}
        value={cert} />
    </div>
  )
}

export default connector(App)
